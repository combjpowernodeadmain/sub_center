import request from '@/utils/request'

export function page(query) {
return request({
url: '/api/test/tUser/page',
method: 'get',
params: query
})
}

export function addObj(obj) {
return request({
url: '/api/test/tUser',
method: 'post',
data: obj
})
}

export function getObj(id) {
return request({
url: '/api/test/tUser/' + id,
method: 'get'
})
}

export function delObj(id) {
return request({
url: '/api/test/tUser/' + id,
method: 'delete'
})
}

export function putObj(id, obj) {
return request({
url: '/api/test/tUser/' + id,
method: 'put',
data: obj
})
}
