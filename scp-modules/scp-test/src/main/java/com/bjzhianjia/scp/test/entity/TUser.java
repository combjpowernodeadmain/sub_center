package com.bjzhianjia.scp.test.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * 
 * 
 * @author rentao
 * @email rentao@qq.com
 * @version 2019-09-03 11:18:57
 */
@Table(name = "t_user")
public class TUser implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Column(name = "name")
    private String name;
	
	    //
    @Id
    private Long id;
	
	    //
    @Column(name = "age")
    private Long age;
	
	    //
    @Column(name = "address")
    private String address;
	

	/**
	 * 设置：
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setAge(Long age) {
		this.age = age;
	}
	/**
	 * 获取：
	 */
	public Long getAge() {
		return age;
	}
	/**
	 * 设置：
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：
	 */
	public String getAddress() {
		return address;
	}
}
