package com.bjzhianjia.scp.test.mapper;

import com.bjzhianjia.scp.test.entity.TUser;
import com.bjzhianjia.scp.security.common.mapper.CommonMapper;

import java.util.List;

/**
 * 
 * 
 * @author rentao
 * @email rentao@qq.com
 * @version 2019-09-03 11:18:57
 */

public interface TUserMapper extends CommonMapper<TUser> {

    @Override
    int updateByPrimaryKeySelective(TUser tUser);

    @Override
    TUser selectByPrimaryKey(Object o);

    @Override
    List<TUser> selectAll();

    @Override
    int insertSelective(TUser tUser);

    @Override
    int deleteByPrimaryKey(Object o);
}
