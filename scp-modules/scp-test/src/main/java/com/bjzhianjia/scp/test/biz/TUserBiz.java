package com.bjzhianjia.scp.test.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjzhianjia.scp.test.entity.TUser;
import com.bjzhianjia.scp.test.mapper.TUserMapper;
import com.bjzhianjia.scp.security.common.biz.BusinessBiz;

/**
 * 
 *
 * @author rentao
 * @email rentao@qq.com
 * @version 2019-09-03 11:18:57
 */
@Service
public class TUserBiz extends BusinessBiz<TUserMapper,TUser> {

    @Autowired
    private TUserMapper tUserMapper;

    @Override
    public TUser selectById(Object id) {
        return tUserMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateSelectiveById(TUser entity) {
        super.updateSelectiveById(entity);
    }
}