package com.bjzhianjia.scp.test.rest;


import com.bjzhianjia.scp.security.common.msg.ObjectRestResponse;
import com.bjzhianjia.scp.security.common.rest.BaseController;
import com.bjzhianjia.scp.test.biz.TUserBiz;
import com.bjzhianjia.scp.test.entity.TUser;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.bjzhianjia.scp.security.auth.client.annotation.CheckClientToken;
import com.bjzhianjia.scp.security.auth.client.annotation.CheckUserToken;

import java.util.List;


@RestController
@RequestMapping("tUser")
@CheckClientToken
@CheckUserToken
public class TUserController extends BaseController<TUserBiz,TUser,Long> {

    @Autowired
    private TUserBiz tUserBiz;

    @ApiOperation("根据id获取用户详细信息")
    @GetMapping("/get/{id}")
    public ObjectRestResponse<TUser> get(@PathVariable Integer id){
        ObjectRestResponse<TUser> entityObjectRestResponse = new ObjectRestResponse<>();
        Object o = tUserBiz.selectById(id);
        TUser TUser = (TUser)o;

        entityObjectRestResponse.data(TUser);

        return entityObjectRestResponse;
    }

    @ApiOperation("获取全部用户详细信息")
    @GetMapping("/getAllUser")
    public ObjectRestResponse<List> getAll(){
        ObjectRestResponse<List> entityObjectRestResponse = new ObjectRestResponse<>();
        List<TUser> tUsers = tUserBiz.selectListAll();
        entityObjectRestResponse.data(tUsers);

        return entityObjectRestResponse;
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation("新增单个对象")
    public ObjectRestResponse<TUser> add(@RequestBody TUser tUser, BindingResult bindingResult){
        ObjectRestResponse<TUser> restResult = new ObjectRestResponse<>();
        if(bindingResult.hasErrors()) {
            restResult.setStatus(400);
            restResult.setMessage(bindingResult.getAllErrors().get(0).getDefaultMessage());
            return restResult;
        }
        tUserBiz.insertSelective(tUser);

        return restResult.data(tUser);
    }

    @RequestMapping(value = "/update/{id}",method = RequestMethod.PUT)
    @ResponseBody
    @ApiOperation("更新单个对象")
    public ObjectRestResponse<TUser> update(@RequestBody @Validated TUser tUser, BindingResult bindingResult){

        ObjectRestResponse<TUser> restResult = new ObjectRestResponse<>();

        if(bindingResult.hasErrors()) {
            restResult.setStatus(400);
            restResult.setMessage(bindingResult.getAllErrors().get(0).getDefaultMessage());
            return restResult;
        }

        tUserBiz.updateSelectiveById(tUser);

        return restResult.data(tUser);
    }

    @RequestMapping(value = "/remove/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    @ApiOperation("指定删除对象")
    public ObjectRestResponse<TUser> remove(@PathVariable Integer id){
        ObjectRestResponse<TUser> result = new ObjectRestResponse<>();
        if(id == null ) {
            result.setStatus(400);;
            result.setMessage("请选择要删除的项");
            return result;
        }
        tUserBiz.deleteById(id);
        return result;
    }
}